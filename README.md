# EmailGenerator

# Introduction

This script will generator email signature with some data input from user.

# Input

* Name
* Role
* Phone number (must be number)
* Email (must be emaul format)
* Website

# Usage

* Input your data and click on button **"Generator Signature"**
* Copy signature is generated on log screen and paste to your email signature edit
* Reset your data by click on button "Reset data"

#Demo 

![](email.gif)

