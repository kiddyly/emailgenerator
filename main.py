def take_user_input():
    email_signature_data = {
        "name": " ",
        "role": " ",
        "email": " ",
        "phone_number": " ",
        "website": " "
    }
    print("Name: ", end = '')
    email_signature_data["name"] = input()
    print("Role: ", end = '')
    email_signature_data["role"] = input()
    print("Phone number: ", end = '')
    email_signature_data["phone_number"] = input()
    print("Email: ", end = '')
    email_signature_data["email"] = input()
    print("Website: ", end = '')
    email_signature_data["website"] = input()
    return email_signature_data

def generate_email_signature_to_file(email_signature_data):
    file = open("email_signature.txt", "w")
    file.write(str(email_signature_data))


if __name__ == "__main__":
    email_signature_data = take_user_input()
    generate_email_signature_to_file(email_signature_data)
    print(email_signature_data)
    

