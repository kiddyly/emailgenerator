const { app, BrowserWindow } = require('electron')

function createWindow() {
    let win = new BrowserWindow({
        width: 600,
        height: 400,
        webPreferences: {
            nodeIntegration: true
        },
        resizable: false ,
        autoHideMenuBar: true
    })
    win.loadFile('index.html')
}
app.on('ready', createWindow)