window.addEventListener("keypress", function(e){
    if(e.keyCode == 13) {
        mainFunction()
    }
})


function onLoadFunction() {
    document.getElementById("myForm__name").focus() 
}

function mainFunction() {
    let name = document.getElementById("myForm__name").value
    let role = document.getElementById("myForm__role").value
    let phone_number = document.getElementById("myForm__phone").value
    let email = document.getElementById("myForm__email").value
    let website = document.getElementById("myForm__website").value
    let checkPoint =  checkValidate(name, role, phone_number, email, website)
    if (checkPoint != false) {
        generateSignature(name, role, phone_number, email, website)
    }
}

function resetDataFunction() {
    document.getElementById("myForm__name").value = ""
    document.getElementById("myForm__role").value = ""
    document.getElementById("myForm__phone").value = ""
    document.getElementById("myForm__email").value = ""
    document.getElementById("myForm__website").value = ""
}

function checkValidate(name, role, phone_number, email, website) {
    // Regex to check validate email
    var emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    // Regex for check string contain number only
    var phoneRegex = /^\d+$/
    if (name == "" || role == "" || phone_number == "" || email == "" || website == "") {
        alert("Please input missed data")
        return false
    }
    if (phoneRegex.test(phone_number) == false) {
        document.getElementById("myForm__phone").value = ""
        alert("Please input right phone number")
        return false
    }
    if (emailRegex.test(email.toLowerCase()) == false) {
        document.getElementById("myForm__email").value = ""
        alert("Please input right email")
        return false
    }


}

function generateSignature(name, role, phone_number, email, website) {
    let name_row = "Name: " + name.bold()
    document.getElementById("container__name").innerHTML = name_row
    let role_row = "Job title: " + role
    document.getElementById("container__role").innerHTML = role_row
    let phone_number_row = "Phone number: " + phone_number
    document.getElementById("container__phone").innerHTML = phone_number_row
    let email_row = "Email: " + email
    document.getElementById("container__email").innerHTML = email_row
    let website_row = "Website: "
    document.getElementById("container__website").innerHTML = website_row + website.link(website)
}